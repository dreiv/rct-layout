import React from 'react'
import { Content } from './Content'
import Sidebar from './Sidebar'

import './Layout.scss'

const Layout = () => {

	return (
		<div className="layout">
			<Sidebar />
			<Content />
		</div>
	)
}

export default Layout
