import React from 'react'

import './Content.scss'

export const Content = () => (
	<main className="container">
		<h1>hello from Content!</h1>
	</main>
)
