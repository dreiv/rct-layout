import React, { useState } from 'react'

import useMedia from 'utils/useMedia'
import c from 'utils/conditionalCls'
import './Sidebar.scss'

const openState = Object.freeze({
	closed: -1,
	initial: 0,
	open: 1,
})

function Sidebar() {
	const [isOpen, toggle] = useState(openState.initial)
	const large = useMedia('(min-width: 62rem)')

	const handleToggle = () => {
		if ((large && isOpen === openState.initial) || isOpen === openState.open) {
			toggle(openState.closed)
		} else if (large && isOpen === openState.closed) {
			toggle(openState.open)
		}
	}

	return (
		<nav className={c`sidebar ${isOpen && 'open'}`}>
			<p>Open state {`${isOpen}`}.</p>
			<button onClick={handleToggle}>Click me</button>
		</nav>
	)
}

export default Sidebar
