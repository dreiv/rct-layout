import React from 'react'

import styles from './Spinner.module.scss'

const Spinner = () => (
	<svg className={styles.spinner} viewBox="0 0 64 64">
		<circle
			className={styles.path}
			cx="25"
			cy="25"
			r="20"
			fill="none"
			stroke-width="5"
		/>
	</svg>
)

export default Spinner
