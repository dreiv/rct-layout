const c = (strings = [], ...classes) =>
	strings.reduce((prev, curr, i) => (prev += curr + (classes[i] || '')), '')

export default c
