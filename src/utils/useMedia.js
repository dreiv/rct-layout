import { useState, useEffect } from 'react'
import debounce from './debounce';

const useMedia = query => {
	let [matches, setMatches] = useState(window.matchMedia(query).matches)

	useEffect(
		() => {
			const media = window.matchMedia(query)
			const listener = debounce(() => setMatches(media.matches))
			media.addListener(listener)
			listener()

			return () => media.removeListener(listener)
		},
		[query]
	)

	return matches
}

export default useMedia
